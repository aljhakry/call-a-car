﻿using System;
using System.Collections.Generic;
using CallaCar.ViewModels;
using Xamarin.Forms;

namespace CallaCar.Views
{
    public partial class OrdersPage : ContentPage
    {
        public OrdersPage()
        {
            InitializeComponent();
            this.BindingContext = new OrdersViewModel();
        }
    }
}
