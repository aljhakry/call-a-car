﻿using System;
using System.Collections.Generic;
using CallaCar.ViewModels;
using Xamarin.Forms;

namespace CallaCar.Views
{
    public partial class OrderDetailsPage : ContentPage
    {
        public OrderDetailsPage()
        {
            InitializeComponent();
            this.BindingContext = new OrderDetailViewModel();
        }
    }
}
