﻿using System;
using System.Collections.Generic;
using CallaCar.ViewModels;
using Xamarin.Forms;

namespace CallaCar.Views
{
    public partial class RegisterPage : ContentPage
    {
        public RegisterPage()
        {
            InitializeComponent();
            this.BindingContext = new RegisterViewModel();
        }
    }
}
