﻿using System;
using System.Collections.Generic;
using CallaCar.ViewModels;
using Xamarin.Forms;

namespace CallaCar.Views
{
    public partial class HomePage : ContentPage
    {
        public HomePage()
        {
            InitializeComponent();
            this.BindingContext = new HomeViewModel();
        }
    }
}
