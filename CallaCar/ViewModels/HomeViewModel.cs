﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Plugin.Fingerprint;
using Shared.Models;
using Xamarin.Forms;

namespace CallaCar.ViewModels
{
    public class HomeViewModel : BaseViewModel
    {
        private bool hasDriven;
        private bool showHome;
        private bool showAddress;
        private bool showWaiting;
        private bool showPanel;
        private bool showPayment;
        private bool should;

        private string streetName;
        private int number;
        private string suffix;
        private string postalCode;
        private string city;

        public Command OrderFromHome { get; }
        public Command OrderFromNewAddress { get; }
        public Command OrderFromPrevAddress { get; }
        public Command GoToWaitingPage { get; }
        public Command PerformAction { get; }
        public Command GoToPayment { get; }

        public HomeViewModel()
        {
            HasDriven = false;
            ShowHome = true;
            ShowAddress = false;
            ShowWaiting = false;
            ShowPanel = false;
            ShowPayment = false;
            Should = true;
            OrderFromHome = new Command(OnFromHomeClicked);
            OrderFromNewAddress = new Command(OnFromNewAddressClicked);
            OrderFromPrevAddress = new Command(OnFromPrevAddressClicked);
            GoToWaitingPage = new Command(OnWaitingPageClicked);
            PerformAction = new Command(OnActionPerformed);
            GoToPayment = new Command(OnEndDriveClicked);
        }

        public bool HasDriven
        {
            get => hasDriven;
            set => SetProperty(ref hasDriven, value);
        }
        public bool ShowHome
        {
            get => showHome;
            set => SetProperty(ref showHome, value);
        }
        public bool ShowAddress
        {
            get => showAddress;
            set => SetProperty(ref showAddress, value);
        }
        public bool ShowWaiting
        {
            get => showWaiting;
            set => SetProperty(ref showWaiting, value);
        }
        public bool ShowPanel
        {
            get => showPanel;
            set => SetProperty(ref showPanel, value);
        }
        public bool ShowPayment
        {
            get => showPayment;
            set => SetProperty(ref showPayment, value);
        }
        public string StreetName
        {
            get => streetName;
            set => SetProperty(ref streetName, value);
        }
        public int Number
        {
            get => number;
            set => SetProperty(ref number, value);
        }
        public string Suffix
        {
            get => suffix;
            set => SetProperty(ref suffix, value);
        }
        public string PostalCode
        {
            get => postalCode;
            set => SetProperty(ref postalCode, value);
        }
        public string City
        {
            get => city;
            set => SetProperty(ref city, value);
        }
        public bool Should
        {
            get => should;
            set => SetProperty(ref should, value);
        }
        public void OnFromHomeClicked()
        {
            ShowHome = false;
            ShowWaiting = true;
            var user = userService.getCurrentUser();
            getCar(user.Address);
            
        }
        public void OnFromNewAddressClicked()
        {
            ShowHome = false;
            ShowAddress = true;
        }
        public void OnFromPrevAddressClicked()
        {
            ShowHome = false;
            ShowWaiting = true;
            var address = userService.getPrevUserAddress();
            getCar(address);
        }
        public void OnWaitingPageClicked()
        {
            ShowAddress = false;
            ShowWaiting = true;
            Address address = new Address
            {
                Id = "12",
                StreetName = StreetName,
                Number = Number,
                Suffix = Suffix,
                PostalCode = PostalCode,
                City = City,
                Country = "Nederland"
            };
            getCar(address);
        }
        public async void getCar(Address address)
        {
            await Task.Delay(3000);
            var result = await carService.SendCar(address);
            if (result && should)
            {
                await App.Current.MainPage.DisplayAlert("Call-a-Car", "De auto is er bijna!", "Ok");
            }
            await Task.Delay(3000);
            ShowWaiting = false;
            ShowPanel = true;
        }
        public async void OnActionPerformed(object obj)
        {
            var result = await carService.SendCommand(obj.ToString());
        }
        public async void OnEndDriveClicked()
        {
            ShowPanel = false;
            ShowPayment = true;
            var isAvailable = await CrossFingerprint.Current.IsAvailableAsync();
            if (isAvailable)
            {
                var authResult = await CrossFingerprint.Current.AuthenticateAsync(
                    new Plugin.Fingerprint.Abstractions.AuthenticationRequestConfiguration("Betalen met vingerafdruk", "pleaaaase"));
                if (authResult.Authenticated)
                {
                    await App.Current.MainPage.DisplayAlert("Bank", "Betaling ontvangen", "Ok");
                    ShowPayment = false;
                    ShowHome = true;
                    HasDriven = true;
                }
            }
        }
    }
}
