﻿using CallaCar.Views;
using System;
using Xamarin.Forms;

namespace CallaCar.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private string phoneNumber;
        private string password;
        public bool loginCalled = false;

        public Command LoginCommand { get; }
        public Command RegisterCommand { get; }

        public LoginViewModel()
        {
            LoginCommand = new Command(OnLoginClicked);
            RegisterCommand = new Command(OnRegisterClicked);
        }

        public async void OnLoginClicked(object obj)
        {
            loginCalled = true;
            if (!String.IsNullOrEmpty(Password) && !String.IsNullOrEmpty(PhoneNumber))
            {
                var user = await userService.Login(PhoneNumber, Password);
                if (user != null)
                {
                    await Shell.Current.GoToAsync($"//{nameof(HomePage)}");
                }
            }
        }

        public bool LoginCalled
        {
            get => loginCalled;
            set => SetProperty(ref loginCalled, value);
        }
        private async void OnRegisterClicked(object obj)
        {
            await Shell.Current.GoToAsync($"//{nameof(RegisterPage)}");
        }

        public string PhoneNumber
        {
            get => phoneNumber;
            set => SetProperty(ref phoneNumber, value);
        }
        public string Password
        {
            get => password;
            set => SetProperty(ref password, value);
        }
    }
}
