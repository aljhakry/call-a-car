﻿using CallaCar.Views;
using Shared.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace CallaCar.ViewModels
{
    public class SettingsViewModel : BaseViewModel
    {
        public Command ShowAddressSettingsPage { get; }
        public Command ShowCarSettingsPage { get; }
        public Command ShowSettingsPage { get; }
        public Command SaveCarSettings { get; }
        public Command GiveAccess { get; }

        private bool showSettings;
        private bool showAddressSettings;
        private bool showCarSettings;

        private string streetName;
        private int number;
        private string suffix;
        private string postalCode;
        private string city;
        private string country;
        private bool should;

        private string type;
        private string brand;
        private bool isTesla;

        public SettingsViewModel()
        {
            ShowSettings = true;
            ShowAddressSettings = false;
            ShowCarSettings = false;

            ShowAddressSettingsPage = new Command(OnAddressSettingsPageClicked);
            ShowCarSettingsPage = new Command(OnCarSettingsPageClicked);
            SaveCarSettings = new Command(OnSaveCarSettingsClicked);
            ShowSettingsPage = new Command(OnGoBackToSettingsClicked);
            GiveAccess = new Command(OnGiveAccess);
            var user = userService.getCurrentUser();
            Should = true;
            Brand = "Tesla";
        }


        public bool ShowSettings
        {
            get => showSettings;
            set => SetProperty(ref showSettings, value);
        }

        public bool ShowAddressSettings
        {
            get => showAddressSettings;
            set => SetProperty(ref showAddressSettings, value);
        }

        public bool ShowCarSettings
        {
            get => showCarSettings;
            set => SetProperty(ref showCarSettings, value);
        }

        public string StreetName
        {
            get => streetName;
            set => SetProperty(ref streetName, value);
        }
        public int Number
        {
            get => number;
            set => SetProperty(ref number, value);
        }
        public string Suffix
        {
            get => suffix;
            set => SetProperty(ref suffix, value);
        }
        public string PostalCode
        {
            get => postalCode;
            set => SetProperty(ref postalCode, value);
        }
        public string City
        {
            get => city;
            set => SetProperty(ref city, value);
        }
        public string Country
        {
            get => country;
            set => SetProperty(ref country, value);
        }
        public bool Should
        {
            get => should;
            set => SetProperty(ref should, value);
        }
        public string Brand
        {
            get => brand;
            set {
                SetProperty(ref brand, value);
                if (value == "Tesla")
                {
                    IsTesla = true;
                } else
                {
                    IsTesla = false;
                }
            }
        }
        public bool IsTesla
        {
            get => isTesla;
            set => SetProperty(ref isTesla, value);
        }

        public string Type
        {
            get => type;
            set => SetProperty(ref type, value);
        }

        public async void OnAddressSettingsPageClicked()
        {
            ShowSettings = false;
            ShowCarSettings = false;
            ShowAddressSettings = true;

            var user = await userService.GetUserDataAsync();

            if (user != null)
            {
                var address = user.Address;
                StreetName = address.StreetName;
                PostalCode = address.PostalCode;
                City = address.City;
                Country = address.Country;
                Number = address.Number;
            }

        }

        public async void OnCarSettingsPageClicked()
        {
            ShowSettings = false;
            ShowAddressSettings = false;
            ShowCarSettings = true;
            var carTypes = await carService.GetCarTypes();
            Debug.WriteLine(carTypes.FirstOrDefault().Brand);
        }

        public async void OnSaveCarSettingsClicked()
        {
            ShowSettings = true;
            ShowAddressSettings = false;
            ShowCarSettings = false;
            Debug.WriteLine("Auto: " + Brand + " ,type: " + Type);
        }
        public void OnGoBackToSettingsClicked()
        {
            ShowSettings = true;
            ShowAddressSettings = false;
            ShowCarSettings = false;
        }
        public async void OnGiveAccess()
        {
            var result = await userService.GiveAccessToEmployee();
            if (result && Should)
            {
                await App.Current.MainPage.DisplayAlert("Call-a-car", "Medewerker toegang gegeven", "Ok");
            }
        }
    }
}