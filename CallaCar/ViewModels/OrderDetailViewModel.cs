﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Shared.Models;
using Xamarin.Forms;

namespace CallaCar.ViewModels
{
    [QueryProperty(nameof(OrderId), nameof(OrderId))]
    public class OrderDetailViewModel : BaseViewModel
    {
        private string orderId;
        private DateTime startTime;
        private DateTime endTime;
        private double price;
        private string startAddres;
        private string endAddres;
        private double amountOfKm;

        private string brand;
        private string type;

        public string Id { get; set; }

        public string OrderId
        {
            get => orderId;
            set
            {
                orderId = value;
                LoadOrderId(value);
            }
        }
        public DateTime StartTime
        {
            get => startTime;
            set => SetProperty(ref startTime, value);
        }

        public DateTime EndTime
        {
            get => endTime;
            set => SetProperty(ref endTime, value);
        }

        public double Price
        {
            get => price;
            set => SetProperty(ref price, value);
        }

        public string StartAddress
        {
            get => startAddres;
            set => SetProperty(ref startAddres, value);
        }
        public string EndAddress
        {
            get => endAddres;
            set => SetProperty(ref endAddres, value);
        }
        public double AmountOfKm
        {
            get => amountOfKm;
            set => SetProperty(ref amountOfKm, value);
        }

        public string Brand

        { 
            get => brand;
            set => SetProperty(ref brand, value);
        }
        public string Type
        {
            get => type;
            set => SetProperty(ref type, value);
        }

        public void LoadOrderId(string id)
        {
            Order order = orderService.GetOrder(id);
            var carType = carService.getCurrentCarType();
            Id = order.Id;
            StartTime = order.StartTime;
            EndTime = order.EndTime;
            Price = order.Price;
            StartAddress = order.StartAddress.StreetName + " " + order.StartAddress.Number + ", " + order.StartAddress.City;
            EndAddress = order.EndAddress.StreetName + " " + order.EndAddress.Number + ", " + order.EndAddress.City;
            AmountOfKm = order.AmountOfKm;
            Brand = carType.Brand;
            Type = carType.Type;
        }
    }
}
