﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using CallaCar.Views;
using Shared.Models;
using Xamarin.Forms;

namespace CallaCar.ViewModels
{
    public class OrdersViewModel : BaseViewModel
    {
        private IEnumerable<Order> orders;
        private Order selectedOrder;
        public Command<Order> OrderTapped { get; }

        public OrdersViewModel()
        {
            getOrders();
            Title = "Ordergeschiedenis";
            OrderTapped = new Command<Order>(OnOrderSelected);
        }

        private async void getOrders()
        {
            var result = await orderService.GetOrders();
            if (result != null)
            {
                Debug.WriteLine("Orders fetched");
                Orders = result;
            }
        }

        public IEnumerable<Order> Orders
        {
            get => orders;
            set => SetProperty(ref orders, value);
        }
        public Order SelectedOrder {
            get => selectedOrder;
            set {
                SetProperty(ref selectedOrder, value);
                OnOrderSelected(value);
            }
        }

        async void OnOrderSelected(Order order)
        {
            if (order.Id == null)
            {
                return;
            }
     
            await Shell.Current.GoToAsync($"{nameof(OrderDetailsPage)}?{nameof(OrderDetailViewModel.OrderId)}={order.Id}");
        }


    }
}
