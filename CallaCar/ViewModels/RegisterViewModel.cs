﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using CallaCar.Views;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace CallaCar.ViewModels
{
    public class RegisterViewModel : BaseViewModel
    {
        private string firstName;
        private string lastName;
        private DateTime dateOfBirth;
        private string phoneNumber;
        private string password;
        private string streetName;
        private int number;
        private string suffix;
        private string postalCode;
        private string city;
        private string country;

        private bool showPersonal;
        private bool showAddress;
        private bool showLicense;
        private bool showConfirmation;

        private string fileName;
        private ImageSource imgSource;
        private bool isLoading;

        public Command ShowPersonalPage { get; }
        public Command ShowAddressPage { get; }
        public Command ShowLicensePage { get;  }
        public Command ShowConfirmationPage { get; }
        public Command PickFileCommand { get; }
        public Command LoginCommand { get; }

        public RegisterViewModel()
        {
            ShowPersonalPage = new Command(OnPersonalPageClicked);
            ShowAddressPage = new Command(OnAddresPageClicked);
            ShowLicensePage = new Command(OnLicensePageClicked);
            ShowConfirmationPage = new Command(OnConfirmationPageClicked);
            PickFileCommand = new Command(OnPickFileClicked);
            LoginCommand = new Command(OnLoginClicked);
            showPersonal = true;
            ShowAddress = false;
            ShowLicense = false;
            ShowConfirmation = false;
            FileName = "";
            IsLoading = false;
            
        }

        public void OnPersonalPageClicked()
        {
            ShowAddress = false;
            ShowPersonal = true;
        }

        public void OnAddresPageClicked() {
            ShowPersonal = false;
            ShowAddress = true;
        }
        public void OnLicensePageClicked() {
            ShowAddress = false;
            ShowLicense = true;
        }
        public async void OnConfirmationPageClicked() {
            ShowLicense = false;
            ShowConfirmation = true;
            IsLoading = true;
            var user = await userService.RegisterUser(FirstName, LastName, DateOfBirth, PhoneNumber, Password, StreetName, Number, Suffix, PostalCode, City, Country);
            if (user != null)
            {
                await Shell.Current.GoToAsync($"//{nameof(HomePage)}");
            }

        }
        public async void OnPickFileClicked()
        {
            try
            {
                var file = await FilePicker.PickAsync();    
                if (file != null)
                {
                    FileName = file.FileName;
                    if (file.FileName.EndsWith("jpg", StringComparison.OrdinalIgnoreCase) ||
                        file.FileName.EndsWith("png", StringComparison.OrdinalIgnoreCase))
                    {
                        var stream = await file.OpenReadAsync();
                        ImgSource = ImageSource.FromStream(() => stream);
                    }
                }
            } catch (Exception)
            {

            }
        }
        public async void OnLoginClicked()
        {
            await Shell.Current.GoToAsync($"//{nameof(LoginPage)}");
        }

        public string FirstName
        {
            get => firstName;
            set => SetProperty(ref firstName, value);
        }
        public string LastName
        {
            get => lastName;
            set => SetProperty(ref lastName, value);
        }
        public DateTime DateOfBirth
        {
            get => dateOfBirth;
            set => SetProperty(ref dateOfBirth, value);
        }
        public string PhoneNumber
        {
            get => phoneNumber;
            set => SetProperty(ref phoneNumber, value);
        }
        public string Password
        {
            get => password;
            set => SetProperty(ref password, value);
        }
        public string StreetName
        {
            get => streetName;
            set => SetProperty(ref streetName, value);
        }
        public int Number
        {
            get => number;
            set => SetProperty(ref number, value);
        }
        public string Suffix
        {
            get => suffix;
            set => SetProperty(ref suffix, value);
        }
        public string PostalCode
        {
            get => postalCode;
            set => SetProperty(ref postalCode, value);
        }
        public string City
        {
            get => city;
            set => SetProperty(ref city, value);
        }
        public string Country
        {
            get => country;
            set => SetProperty(ref country, value);
        }

        public bool ShowPersonal
        {
            get => showPersonal;
            set => SetProperty(ref showPersonal, value);
        }

        public bool ShowAddress
        {
            get => showAddress;
            set => SetProperty(ref showAddress, value);
        }

        public bool ShowLicense
        {
            get => showLicense;
            set => SetProperty(ref showLicense, value);
        }

        public bool ShowConfirmation
        {
            get => showConfirmation;
            set => SetProperty(ref showConfirmation, value);
        }
        public string FileName
        {
            get => fileName;
            set => SetProperty(ref fileName, value);
        }
        public ImageSource ImgSource
        {
            get => imgSource;
            set => SetProperty(ref imgSource, value);
        }
        public bool IsLoading
        {
            get => isLoading;
            set => SetProperty(ref isLoading, value);
        }
    }
}
