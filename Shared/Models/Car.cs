﻿using System;
namespace Shared.Models
{
    public class Car
    {
        public string Id { get; set; }
        public string CarTypeId { get; set; }
        public string ParkingFacilityId { get; set; }
    }
}
