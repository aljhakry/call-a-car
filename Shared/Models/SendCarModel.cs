﻿using System;
namespace Shared.Models
{
    public class SendCarModel
    {
        
        public CarType CarType { get; set; }
        public Address Address { get; set; }

        public SendCarModel(CarType carType, Address address)
        {
            CarType = carType;
            Address = address;
        }   
    }
}
