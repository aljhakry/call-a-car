﻿using System;
namespace Shared.Models
{
    public class CarType
    {
        public string Id { get; set; }
        public string Brand { get; set; }
        public string Type { get; set; }
        public int NumberOfSeats { get; set; }
        public bool WeelchairSpace { get; set; }
    }
}
