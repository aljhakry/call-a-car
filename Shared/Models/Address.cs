﻿using System;
namespace Shared.Models
{
    public class Address
    {
        public string Id { get; set; }
        public string StreetName { get; set; }
        public int Number { get; set; }
        public string Suffix { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}
