﻿using System;
namespace Shared.Models
{
    public class Order
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string CarId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public double Price { get; set; }
        public Address StartAddress { get; set; }
        public Address EndAddress { get; set; }
        public double AmountOfKm { get; set; }
    }
}
