using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Shared.Models;
using Newtonsoft.Json;
using System.Text;

namespace Shared.Services
{
    public class UserService
    {
        private User currentUser;
        private HttpClient httpClient;
        private string url = "https://call-a-car-api.herokuapp.com/User/";

        public UserService()
        {
            httpClient = new HttpClient();
            currentUser = new User
            {
                Id = "1",
                FirstName = "Bob",
                LastName = "Berg",
                Address = new Address
                {
                    Id = "1",
                    City = "Breda",
                    Country = "Nederland",
                    Number = 5,
                    PostalCode = "4811HL",
                    StreetName = "Keizerstraat"
                },
                PhoneNumber = "0612345678",
                DateOfBirth = new DateTime(2000, 1, 1)
            };
        }

        public async Task<User> GetUserDataAsync()
        {

            Uri uri = new Uri(url + "1");

            HttpResponseMessage response = await httpClient.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var responseRead = await response.Content.ReadAsStringAsync();
                var objResponse = JsonConvert.DeserializeObject<User>(responseRead);
                return objResponse;
            }
            else
            {
                throw new NotImplementedException();
            }



}

        public async Task<User> RegisterUser(string FirstName, string LastName, DateTime DateOfBirth, string PhoneNumber, string Password, string StreetName, int Number, string Suffix, string PostalCode, string City, string Country)
        {
            try
            {
                var address = new Address()
                {
                    Id = Guid.NewGuid().ToString(),
                    StreetName = StreetName,
                    Number = Number,
                    Suffix = Suffix,
                    PostalCode = PostalCode,
                    City = City,
                    Country = Country
                };
                var user = new User()
                {
                    Id = Guid.NewGuid().ToString(),
                    FirstName = FirstName,
                    LastName = LastName,
                    DateOfBirth = DateOfBirth,
                    PhoneNumber = PhoneNumber,
                    Address = address
                };
                Uri uri = new Uri(url);
                var stringContent = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json");
                var response = await httpClient.PostAsync(url, stringContent);
                if (response.IsSuccessStatusCode)
                {
                    var responseRead = await response.Content.ReadAsStringAsync();
                    var objResponse = JsonConvert.DeserializeObject<User>(responseRead);
                    currentUser = objResponse;
                    return objResponse;
                }
                else
                {
                    Debug.WriteLine("Failed to add user");
                    return null;                
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return null;
            }
        }
        public async Task<User> Login(string PhoneNumber, string Password)
        {
            var body = new LoginModel(PhoneNumber, Password);
            var stringContent = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
            var response = await httpClient.PostAsync(url + "login", stringContent);
            if (response.IsSuccessStatusCode)
            {
                var responseRead = await response.Content.ReadAsStringAsync();
                var objResponse = JsonConvert.DeserializeObject<User>(responseRead);
                currentUser = objResponse;
                return objResponse;
            }
            else
            {
                Debug.WriteLine("Failed to login user");
                return null;
            }
        }

        public User getCurrentUser()
        {
            return currentUser;
        }

        public async Task<bool> GiveAccessToEmployee()
        {
            try
            {
                Uri uri = new Uri(url + "access");
                HttpResponseMessage response = await httpClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var responseRead = await response.Content.ReadAsStringAsync();
                    var objResponse = JsonConvert.DeserializeObject<bool>(responseRead);
                    return objResponse;
                }
                else
                {
                    return false;
                }
            }catch
            {
                return true;
            }

        }

        public bool Logout()
        {
            currentUser = null;
            return true;
        }

        public Address getPrevUserAddress()
        {
            return currentUser.Address;
        }
    }
}
