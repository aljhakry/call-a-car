﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Shared.Models;

namespace Shared.Services
{
    public class CarService
    {
        private CarType currentCarType;
        private HttpClient httpClient;
        private string url = "https://call-a-car-api.herokuapp.com/Car/";

        public CarService()
        {
            httpClient = new HttpClient();
            currentCarType = new CarType
            {
                Id = "1",
                Brand = "Tesla",
                Type = "Model X",
                NumberOfSeats = 7,
                WeelchairSpace = true
            };
        }

        public CarType getCurrentCarType()
        {
            return currentCarType;
        }
        public async Task<IEnumerable<CarType>> GetCarTypes()
        {
            Uri uri = new Uri(url + "types");
            HttpResponseMessage response = await httpClient.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var responseRead = await response.Content.ReadAsStringAsync();
                var objResponse = JsonConvert.DeserializeObject<IEnumerable<CarType>>(responseRead);
                return objResponse;
            }
            else
            {
                return null;
            }
        }
        public async Task<bool> SendCommand(string command)
        {
            Uri uri = new Uri(url + "command/" + command);
            HttpResponseMessage response = await httpClient.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var responseRead = await response.Content.ReadAsStringAsync();
                var objResponse = JsonConvert.DeserializeObject<bool>(responseRead);
                return objResponse;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> SendCar(Address address)
        {
            var body = new SendCarModel(currentCarType, address);
            var stringContent = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
            var response = await httpClient.PostAsync(url + "send", stringContent);
            if (response.IsSuccessStatusCode)
            {
                var responseRead = await response.Content.ReadAsStringAsync();
                var objResponse = JsonConvert.DeserializeObject<bool>(responseRead);
                return objResponse;
            }
            else
            {
                return false;
            }
        }

        public bool SaveCar(string brand, string type, int numOfSeats, bool weelchair)
        {
            try
            {
                var carType = new CarType
                {
                    Id = "12",
                    Brand = brand,
                    Type = type,
                    NumberOfSeats = numOfSeats,
                    WeelchairSpace = weelchair
                };
                return true;

            } catch
            {
                return false;
            }
        }
    }
}
