﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Shared.Models;

namespace Shared.Services
{
    public class OrderService
    {

        private IEnumerable<Order> orders;
        private Order order;

        private HttpClient httpClient;
        private string url = "https://call-a-car-api.herokuapp.com/Order/";

        public OrderService()
        {
            httpClient = new HttpClient();
            order = new Order
            {
                Id = "1",
                UserId = "1",
                CarId = "1",
                StartTime = new DateTime(2021, 03, 07, 12, 30, 30),
                EndTime = new DateTime(2021, 03, 07, 14, 30, 30),
                Price = 4.50,
                StartAddress = new Address
                {
                    Id = "2",
                    City = "Rotterdam",
                    Country = "Nederland",
                    Number = 6,
                    PostalCode = "3066LL",
                    StreetName = "Pannenkoekstraat"
                },
                EndAddress = new Address
                {
                    Id = "3",
                    City = "Utrecht",
                    Country = "Nederland",
                    Number = 5,
                    PostalCode = "2020KM",
                    StreetName = "Jaarbeurs"
                },
                AmountOfKm = 5.47
            };
        }

        public async Task<IEnumerable<Order>> GetOrders()
        {
            Uri uri = new Uri(url);
            HttpResponseMessage response = await httpClient.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var responseRead = await response.Content.ReadAsStringAsync();
                var objResponse = JsonConvert.DeserializeObject<IEnumerable<Order>>(responseRead);
                orders = objResponse;
                return objResponse;
            }
            else
            {
                return null;
            }
        }

        public async Task<IEnumerable<Order>> AddOrder(Order order)
        {
            var stringContent = new StringContent(JsonConvert.SerializeObject(order), Encoding.UTF8, "application/json");
            var response = await httpClient.PostAsync(url, stringContent);
            if (response.IsSuccessStatusCode)
            {
                var responseRead = await response.Content.ReadAsStringAsync();
                var objResponse = JsonConvert.DeserializeObject<IEnumerable<Order>>(responseRead);
                return objResponse;
            }
            else
            {
                return null;
            }
        }

        public Order GetOrder(string id)
        {
            return order;
        }
    }
}
