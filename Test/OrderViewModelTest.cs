﻿using System;
using CallaCar.ViewModels;
using Xunit;

namespace Test
{
    public class OrderViewModelTest
    {
        [Fact]
        public void TestIfWorks()
        {
            var viewmodel = new OrdersViewModel();
            Assert.NotNull(viewmodel.OrderTapped);
            Assert.Null(viewmodel.Orders);
            Assert.Equal("Ordergeschiedenis", viewmodel.Title);
        }
    }
}
