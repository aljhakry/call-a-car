﻿using System;
using CallaCar.ViewModels;
using Moq;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xunit;

namespace Test
{
    public class RegisterViewModelTest
    {

        [Fact]
        public void TestIfContactPageIsVisibleAtStartup() {
            var viewModel = new RegisterViewModel();
            Assert.True(viewModel.ShowPersonal);
            Assert.False(viewModel.ShowAddress);
            Assert.False(viewModel.ShowLicense);
            Assert.False(viewModel.ShowConfirmation);
        }

        [Fact]
        public void TestIfAddressPageIsVisibleAfterShowAddresPageCommand()
        {
            var viewModel = new RegisterViewModel();
            viewModel.ShowAddressPage.Execute(new Object());
            Assert.False(viewModel.ShowPersonal);
            Assert.True(viewModel.ShowAddress);
            Assert.False(viewModel.ShowLicense);
            Assert.False(viewModel.ShowConfirmation);

        }
        [Fact]
        public void TestIfContactPageIsVisibleAfterReturnCommand()
        {
            var viewModel = new RegisterViewModel();
            viewModel.ShowAddressPage.Execute(new Object());
            viewModel.ShowPersonalPage.Execute(new Object());
            Assert.True(viewModel.ShowPersonal);
            Assert.False(viewModel.ShowAddress);
            Assert.False(viewModel.ShowLicense);
            Assert.False(viewModel.ShowConfirmation);
        }

        [Fact]
        public void TestIfLicensePageIsVisibleAfterShowLicensePageCommand()
        {
            var viewModel = new RegisterViewModel();
            viewModel.ShowAddressPage.Execute(new Object());
            viewModel.ShowLicensePage.Execute(new Object());
            Assert.False(viewModel.ShowPersonal);
            Assert.False(viewModel.ShowAddress);
            Assert.True(viewModel.ShowLicense);
            Assert.False(viewModel.ShowConfirmation);
        }

        [Fact]
        public void TestIfIsLoadingIsFalseAtStartup()
        {
            var viewModel = new RegisterViewModel();
            Assert.False(viewModel.IsLoading);
            Assert.Empty(viewModel.FileName);
            Assert.Null(viewModel.ImgSource);
        }

        [Fact]
        public void TestIfUserCanRegister()
        {
            var viewModel = new RegisterViewModel();
            viewModel.FirstName = "Alex";
            viewModel.LastName = "Jansen";
            viewModel.DateOfBirth = new DateTime(1970, 12, 2);
            viewModel.PhoneNumber = "061234567";
            viewModel.Password = "Wachtwoord";
            viewModel.StreetName = "Pannenkoekstraat";
            viewModel.Number = 22;
            viewModel.Suffix = "a";
            viewModel.PostalCode = "3000KK";
            viewModel.City = "Rotterdam";
            viewModel.Country = "Nederland";
            viewModel.ShowAddressPage.Execute(new Object());
            viewModel.ShowLicensePage.Execute(new Object());
            viewModel.ShowLicense = false;
            viewModel.ShowConfirmation = true;
            var dateofBith = viewModel.DateOfBirth;
            Assert.False(viewModel.ShowPersonal);
            Assert.False(viewModel.ShowAddress);
            Assert.False(viewModel.ShowLicense);
            Assert.True(viewModel.ShowConfirmation);
            Assert.Equal("Alex", viewModel.FirstName);
            Assert.Equal("Jansen", viewModel.LastName);
            Assert.Equal(dateofBith, viewModel.DateOfBirth);
            Assert.Equal("061234567", viewModel.PhoneNumber);
            Assert.Equal("Wachtwoord", viewModel.Password);
            Assert.Equal("Pannenkoekstraat", viewModel.StreetName);
            Assert.Equal(22, viewModel.Number);
            Assert.Equal("a", viewModel.Suffix);
            Assert.Equal("3000KK", viewModel.PostalCode);
            Assert.Equal("Rotterdam", viewModel.City);
            Assert.Equal("Nederland", viewModel.Country);

        }

        [Fact]
        public void canPickFile()
        {
            var viewmodel = new RegisterViewModel();
            viewmodel.OnPickFileClicked();
            Assert.True(true);
        }
    }
}
