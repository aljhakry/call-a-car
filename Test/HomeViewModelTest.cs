﻿using System;
using CallaCar.ViewModels;
using Xamarin.Forms;
using Xunit;

namespace Test
{
    public class HomeViewModelTest
    {

        [Fact]
        public void HasTheRightStartupValue()
        {
            var viewmodel = new HomeViewModel();
            Assert.False(viewmodel.HasDriven);
            Assert.True(viewmodel.ShowHome);
            Assert.False(viewmodel.ShowAddress);
            Assert.False(viewmodel.ShowWaiting);
            Assert.False(viewmodel.ShowPanel);
            Assert.False(viewmodel.ShowPayment);
            Assert.True(viewmodel.Should);
            Assert.NotNull(viewmodel.OrderFromHome);
            Assert.NotNull(viewmodel.OrderFromNewAddress);
            Assert.NotNull(viewmodel.OrderFromPrevAddress);
            Assert.NotNull(viewmodel.GoToWaitingPage);
            Assert.NotNull(viewmodel.GoToPayment);
            Assert.NotNull(viewmodel.PerformAction);
        }

        [Fact]
        public void TestOrderCarFromHome()
        {
            var viewmodel = new HomeViewModel();
            viewmodel.Should = false;
            viewmodel.OnFromHomeClicked();
            Assert.True(viewmodel.ShowWaiting);
            Assert.False(viewmodel.ShowHome);
        }

        [Fact]
        public void TestOrderCarNewAddress()
        {
            var viewmodel = new HomeViewModel();
            viewmodel.Should = false;
            viewmodel.OnFromNewAddressClicked();
            Assert.True(viewmodel.ShowAddress);
            Assert.False(viewmodel.ShowHome);
        }

        [Fact]
        public void TestOrderFromAddress()
        {
            var viewmodel = new HomeViewModel();
            viewmodel.Should = false;
            viewmodel.OnFromPrevAddressClicked();
            Assert.True(viewmodel.ShowWaiting);
            Assert.False(viewmodel.ShowHome);
        }
        [Fact]
        public void TestWaitingPageClicked()
        {
            var viewmodel = new HomeViewModel();
            viewmodel.StreetName = "Pannenkoekstraat";
            viewmodel.Number = 22;
            viewmodel.Suffix = "a";
            viewmodel.PostalCode = "3030LL";
            viewmodel.City = "Rotterdam";
            viewmodel.Should = false;
            viewmodel.OnWaitingPageClicked();
            Assert.False(viewmodel.ShowAddress);
            Assert.True(viewmodel.ShowWaiting);
            Assert.Equal("Pannenkoekstraat", viewmodel.StreetName);
            Assert.Equal(22, viewmodel.Number);
            Assert.Equal("a", viewmodel.Suffix);
            Assert.Equal("3030LL", viewmodel.PostalCode);
            Assert.Equal("Rotterdam", viewmodel.City);
        }
    }
}
