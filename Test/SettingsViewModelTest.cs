﻿using System;
using CallaCar.ViewModels;
using Xunit;
namespace Test
{
    public class SettingsViewModelTest
    {
        [Fact]
        public void TestIfSettingsViewModelHasNoValuesAtStartUp()
        {
            var viewModel = new SettingsViewModel();
            Assert.True(viewModel.ShowSettings);
            Assert.False(viewModel.ShowAddressSettings);
            Assert.False(viewModel.ShowCarSettings);
            Assert.Null(viewModel.StreetName);
            Assert.Null(viewModel.Suffix);
            Assert.Null(viewModel.PostalCode);
            Assert.Null(viewModel.City);
            Assert.Null(viewModel.Country);
            Assert.True(viewModel.Should);
            Assert.NotNull(viewModel.ShowAddressSettingsPage);
            Assert.NotNull(viewModel.ShowCarSettingsPage);
            Assert.NotNull(viewModel.ShowSettingsPage);
            Assert.NotNull(viewModel.SaveCarSettings);
            Assert.NotNull(viewModel.GiveAccess);
        }

        [Fact]
        public void TestIfParametersChangeAfterClickAddressPage()
        {
            var viewModel = new SettingsViewModel();
            viewModel.OnAddressSettingsPageClicked();
            Assert.False(viewModel.ShowSettings);
            Assert.True(viewModel.ShowAddressSettings);
            Assert.False(viewModel.ShowCarSettings);
        }

        [Fact]
        public void TestIfParametersChangeAfterOnCarPageClicked()
        {
            var viewModel = new SettingsViewModel();
            viewModel.OnCarSettingsPageClicked();
            Assert.False(viewModel.ShowSettings);
            Assert.False(viewModel.ShowAddressSettings);
            Assert.True(viewModel.ShowCarSettings);
        }


        [Fact]
        public void TestIfParametersChangeAfterOnCarSaveClicked()
        {
            var viewModel = new SettingsViewModel();
            viewModel.OnSaveCarSettingsClicked();
            Assert.True(viewModel.ShowSettings);
            Assert.False(viewModel.ShowAddressSettings);
            Assert.False(viewModel.ShowCarSettings);
        }

        [Fact]
        public void TestIfParametersChangeAfterOnBackSettingsClicked()
        {
            var viewModel = new SettingsViewModel();
            viewModel.OnGoBackToSettingsClicked();
            Assert.True(viewModel.ShowSettings);
            Assert.False(viewModel.ShowAddressSettings);
            Assert.False(viewModel.ShowCarSettings);
        }

        [Fact]
        public void testAccess()
        {
            var viewmodel = new SettingsViewModel();
            viewmodel.Should = false;
            viewmodel.OnGiveAccess();
            Assert.False(viewmodel.Should);
        }

    }
}
