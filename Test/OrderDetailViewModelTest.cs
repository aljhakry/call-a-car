﻿using System;
using CallaCar.ViewModels;
using Xunit;

namespace Test
{
    public class OrderDetailViewModelTest
    {
        [Fact]
        public void canSetProperties()
        {
            var viewmodel = new OrderDetailViewModel();
            viewmodel.OrderId = "1";
            Assert.Equal("1",viewmodel.Id);
            Assert.Equal(4.50, viewmodel.Price);
        }
    }
}
