﻿using System;
using CallaCar;
using CallaCar.ViewModels;
using CallaCar.Views;
using Xamarin.Forms;
using Xunit;
namespace Test
{
    public class LoginViewModelTest
    {

        [Fact]
        public void TestIfLoginViewModelHasNoValuesAtStartUp()
        {
            var viewModel = new LoginViewModel();
            Assert.Null(viewModel.PhoneNumber);
            Assert.Null(viewModel.Password);
        }
        [Fact]
        public void TestIfLoginWorks()
        {
            var viewModel = new LoginViewModel();
            viewModel.PhoneNumber = "061235678";
            viewModel.Password = "Appels";
            viewModel.OnLoginClicked(new Object());
            Assert.True(viewModel.LoginCalled);
        }
    }
}
